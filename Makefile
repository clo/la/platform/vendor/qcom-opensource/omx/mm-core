all:
	@echo "invoking omxcore make"
	$(MAKE) -C omxcore

install:
	$(MAKE) -C omxcore install

.PHONY: clean
clean:
	$(MAKE) -C omxcore clean
